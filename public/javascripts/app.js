var app = angular.module("app", ['ngRoute']);
app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
    when('/userlist', {
        templateUrl: 'templates/user-list.html',
        controller: 'AppCtrl'
    }).
    when('/editcustomer/:custID', {
        templateUrl: 'templates/user-add.html',
        controller: 'DetailsController'
    }).
    when('/adduser', {
        templateUrl: 'templates/user-add.html',
        controller: 'AppCtrl'
    }).
    when('/showcustomer/:custID', {
        templateUrl: 'templates/user-show.html',
        controller: 'DetailsController'
    }).
      otherwise({
        redirectTo: '/userlist'
      });
  }]);

app.directive('jqdatepicker', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
         link: function (scope, element, attrs, ngModelCtrl) {
            element.datepicker({
                dateFormat: "dd/mm/yy",
                onSelect: function (date) {
                    scope.date = date;
                    scope.$apply();
                }
            });
        }
    };
});


app.controller("DetailsController", ['$scope','$http','$routeParams', '$filter',
    function($scope, $http, $routeParams, $filter)
        {    
            $scope.user = $scope.people[$routeParams.custID];
            $scope.user.dateOfBirth =  $filter('date')($scope.user.dateOfBirth, 'dd/MM/yyyy');
            $scope.action = "Save";
            
            $scope.addChangesToPerson = function() {  
                if (typeof $scope.date != 'undefined') {
                    $scope.showData = $scope.date;
                    $scope.date =  $scope.date.split("/");
                    $scope.date = $scope.date[1]+"/"+$scope.date[0]+"/"+$scope.date[2];
                    $scope.user.dateOfBirth = $scope.date;
                }else{
                    $scope.showData = $scope.user.dateOfBirth;
                }
                
                $http.post("http://localhost:3000/update", $scope.user)
                .success(function(data) {
                  $scope.user.dateOfBirth = $scope.showData;
                });
            }
        }]
);


app.controller("AppCtrl", function( $scope, $http, $location ) {
   
   
   $scope.clearSource = function(){
        $scope.user   = {};
    };
    
    $scope.initFirst = function(){
       $http.get("http://localhost:3000/list")
       .success(function(data) {
         $scope.people = data;
         $scope.user   = {};
         $scope.action = "Create";
       });
    };

/**
 * add customer to list
 * @returns {undefined}
 */
    $scope.addChangesToPerson = function() {      
        $http.post("http://localhost:3000/save", $scope.user)
        .success(function(data) {
          $scope.people.push($scope.user);  
          $scope.user = {};  
          alert("User was added");
          //$location.path('/userlist');
        });
    }

    $scope.deletePerson = function(person) {
        $http.post("http://localhost:3000/delete", person)
        .success(function(data) {
            $scope.people.splice( $scope.people.indexOf(person), 1 );
        });
        $location.path('#/adduser');
    } 
});