
var express  = require('express');
var extend   = require('mongoose-schema-extend');
var mongoose = require('mongoose');

var Schema   = mongoose.Schema;
var app      = module.exports = express.createServer();

mongoose.connect('mongodb://localhost/users',function(error){
    if(error)
        console.log("Error" + " " + error);   
});

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true }));
});

app.configure('production', function(){
  app.use(express.errorHandler());
});

var PersonSchema = new Schema({
    name: {
      first: String,
      last: String
    },
    dateOfBirth: { type: Date, default: Date.now }
});

var CustomerSchema = PersonSchema.extend({
    companyName: String,
    phone: {
        mobile: String,
        work: String
    },
    skype: String
});


var Persons = mongoose.model('persons', PersonSchema);
var Users = mongoose.model('users', PersonSchema);

var Customer = mongoose.model('customer', CustomerSchema);

app.get('/list', function(req, res, next) {
    Customer.find(function(err, customers){
        if(err){ return next(err); }
        res.send(customers);
    });
});

app.post('/save', function(request, response){
    
    var cutomer = new Customer({
        name:request.body.name,
        dateOfBirth: request.body.dateOfBirth,
        companyName: request.body.companyname,
        phone: request.body.phone,
        skype: request.body.skype
    });
    cutomer.save(function(err){
        if(err){
            console.log(err);
        }
    });
    response.send(200);
});


app.post('/delete', function(request, response){
   // response.send({name:request.body._id});
    Customer.findByIdAndRemove( request.body._id, function(err){
        if(err){ response.send(404); }
    });
    response.send(200);
});

app.post('/update', function(request, response){
    Customer.findById( request.body._id, function ( err, customer ){
    customer.name = request.body.name;
    customer.dateOfBirth = request.body.dateOfBirth;
    customer.companyName = request.body.companyName;
    customer.phone = request.body.phone;
    customer.skype = request.body.skype;
    customer.save( function ( err, todo, count ){
      response.send(200);
    });
  });
    //response.send(404);
});



app.listen(3000, function(){
  console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
});
